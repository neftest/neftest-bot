$Env:NEFTEST_GROUP_ID = '16053802';
$Env:NEFTEST_BOT_USER_ID = '10967171';

# helpers
. ".\lib\git-helpers.ps1"
. ".\lib\gitlab-api.ps1"

# rules
. ".\rules\dependencies.ps1"
. ".\rules\pipelines.ps1"
. ".\rules\approvals.ps1"
. ".\rules\assembly-props.ps1"

# global vars
# these will be filled in throughout

$MERGEREQUEST_RESULTS = @{}

# load all projects
$projectLookup = Get-GitlabGroupProjects $Env:NEFTEST_GROUP_ID

# load all project approval rules
# $projectApprovalRules = Get-ProjectApprovalRules $projectLookup

# load all opened milestones
$mergeRequests = Get-GitlabGroupMergeRequests -GroupId $Env:NEFTEST_GROUP_ID -State 'opened'

# check each milestone to see if we can/should approve it
foreach ($mergeRequest in $mergeRequests) {
    $project = $projectLookup[$mergeRequest.project_id]

    Write-Host "Checking Merge Request: $($mergeRequest.title) in $($project.name):"

    # holds our global merge request state
    $MERGEREQUEST_RESULTS[$mergeRequest.id] = @{
        note = $false
        thread = $false
        pipeline = $false
        failedTasks = @()
        passedTasks = @()
    }

    # adds bot as a reviewer to any open merge requests
    Add-MergeRequestReviewers $mergeRequest

    # grab the merge request branch
    Checkout-MergeRequest $mergeRequest
    
    # validate assemblyinfo
    Check-AssemblyInfo $mergeRequest
    
    # checks for valid project dependencies
    # Check-ProjectDependencies -MergeRequest $mergeRequest

    # check that we have a passing pipeline for the current sha
    Check-MergeRequestPipelines $mergeRequest

    # check that pipeline artifacts are good
    Check-PipelineArtifacts $mergeRequest

    # check that this merge request has all needed approvals (excluding our own)
    Check-MergeRequestApprovals $mergeRequest

    # make sure all tasklist items are unique
    $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks = $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks | select -Unique
    $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks = $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks | select -Unique
    
    Write-Host "  - Failed Tasks: "
    foreach ($task in $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks) {
        Write-Host "    - $task"
    }
    Write-Host "  - Passed Tasks: "
    foreach ($task in $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks) {
        Write-Host "    - $task"
    }

    # creates/updates thread on merge request with any issues
    Update-MergeRequestThread $mergeRequest

    # if we have failed tasks we bail early
    if ($MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count -gt 0) {
        Write-Host "  - Skipping potential merge request because some tasks failed"
        continue
    }

    # if this is a draft or wip we bail early
    if ($mergeRequest.draft -or $mergeRequest.work_in_progress -or $mergeRequest.merge_status -ne 'can_be_merged') {
        Write-Host "  - Skipping potential merge because: draft:$($mergeRequest.draft) wip:$($mergeRequest.work_in_progress) merge_status:$($mergeRequest.merge_status)"
        continue
    }

    # if we made it this far, we can actually merge the request

    # resolve our thread
    Resolve-MergeRequestThread $mergeRequest $true

    # give our approval
    Approve-MergeRequest $mergeRequest

    # merge
    Merge-MergeRequest $mergeRequest
    
}
