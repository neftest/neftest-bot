﻿function Get-ReposPath() {
    return ".\repos\"
}

function Get-MergeRequestRepoPath($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    return "$(Get-ReposPath)$($project.path)-$($mergeRequest.source_branch)-$($mergeRequest.sha)\"
}

function CheckOut-GitlabBranch([string]$Branch, [string]$GitRepoUri, [string]$Path) {
    # create repos parent directory if it doesnt exist
    if (!(Test-Path -Path $(Get-ReposPath))) {
        New-Item -ItemType Directory -Name $(Get-ReposPath)
    }

    # delete existing repo dir for project if it exists
    if ((Test-Path -Path $Path)) {
        Remove-Item -Force -Confirm:$false -Recurse -Path $Path
    }
    
    # clone branch
    Write-Host "Cloning Repository: $GitRepoUri (Branch: $Branch)"
    git clone --branch $Branch $GitRepoUri $Path
}

function Checkout-MergeRequest($mergeRequest) {
    $repoPath = Get-MergeRequestRepoPath $mergeRequest
    $project = $projectLookup[$mergeRequest.project_id]

    CheckOut-GitlabBranch -Branch $($mergeRequest.source_branch) -GitRepoUri $($project.http_url_to_repo) -Path $repoPath

    return $repoPath
}