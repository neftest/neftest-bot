function Update-GitlabItem([string]$Uri, $Body) {
    $_headers = @{
        'PRIVATE-TOKEN' = $Env:BOT_API_TOKEN
    }

    $_postJson = ($Body | ConvertTo-Json -Depth 10)

    $_response = Invoke-WebRequest -Headers $_headers -Method Put -ContentType "application/json" -Body $_postJson -Uri "${Uri}"

    return $_response
}

function Create-GitlabItem([string]$Uri, $Body) {
    $_headers = @{
        'PRIVATE-TOKEN' = $Env:BOT_API_TOKEN
    }

    $_postJson = ($Body | ConvertTo-Json -Depth 10)

    $_response = Invoke-WebRequest -Headers $_headers -Method Post -ContentType "application/json" -Body $_postJson -Uri "${Uri}"

    return $_response
}

function Get-GitlabItem([string]$Uri) {
    $_headers = @{
        'PRIVATE-TOKEN' = $Env:BOT_API_TOKEN
    }

    $_response = Invoke-WebRequest -Headers $_headers -Uri "${Uri}"

    return $_response | ConvertFrom-Json
}

function Get-GitlabEndpointPagesItems([string]$endpoint, [int]$page, [bool]$recurse) {
	$_found = @()
    $_headers = @{
        'PRIVATE-TOKEN' = $Env:BOT_API_TOKEN
    }

    $joinchar = "?"
    if ($endpoint.Contains("?")) {
        $joinchar = "&"
    }
    
    #Write-Host "Fetching Endpoint Page: (page $page) ${endpoint}${joinchar}per_page=100&page=${page}"
	$_response = Invoke-WebRequest -Headers $_headers -Uri "${endpoint}${joinchar}per_page=100&page=${page}"
	$_responseJson = ConvertFrom-Json $([String]::new($_response.Content))

	foreach ($item in $_responseJson) {
		$_found += $item
	}

	if ($recurse) {
		for ($page=2; $page -le [Int]$_response.Headers['x-total-pages']; $page++) {
			$_found += Get-GitlabEndpointPagesItems $endpoint $page 0
		}
	}

	return $_found
}

function Get-GitlabEndpointItems([string]$endpoint) {
    #Write-Host "Fetching Endpoint:  $endpoint"
	return Get-GitlabEndpointPagesItems $endpoint 1 1
}

function Get-GitlabGroupMergeRequests([string]$groupId, [string]$state) {
    Write-Host "Fetching all group merge requests: $groupId $state"
	return Get-GitlabEndpointItems "https://gitlab.com/api/v4/groups/${groupId}/merge_requests?state=${state}"
}

function Get-GitlabGroupProjects([string]$groupId) {
    Write-Host "Fetching all group projects: $groupId"
	$projects = Get-GitlabEndpointItems "https://gitlab.com/api/v4/groups/${groupId}/projects"

    # index projects by id for easy lookup later
    $projectLookup = @{}
    foreach ($project in $projects) {
        Write-Host "Found Project: $($project.name)"
        $projectLookup[$project.id] = $project
    }

    return $projectLookup
}

<#
    Add the bot as a merge request reviewer if it doesnt already exist
#>
function Add-MergeRequestReviewers($mergeRequest) {
    if (!@([array]$mergeRequest.reviewers | %{ [int]$_.id }).Contains([int]$Env:NEFTEST_BOT_USER_ID)) {
        $reviewerIds = [array]@();

        Write-Host "  - Adding bot as a reviewer"

        if (([array]$mergeRequest.reviewers).Count -gt 0) {
            foreach ($reviewer in [array]$mergeRequest.reviewers) {
                $reviewerIds += [int]$reviewer.id
            }
        }

        $reviewerIds += [int]$Env:NEFTEST_BOT_USER_ID
        
        $postBody = @{
            reviewer_ids = $reviewerIds
        }

        $_response = Update-GitlabItem -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)" -body $postBody
    }
    else {
        Write-Host "  - Bot is already a reviewer"
    }
}

function Find-ExistingMergeRequestThread($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    

    $threads = Get-GitlabEndpointItems "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/discussions"
    $foundNote = $false
    $foundThread = $false
    foreach ($thread in $threads) {
        if ($thread.individual_note -eq $false) {
            foreach ($note in $thread.notes) {
                if ([string]$note.author.id -eq $Env:NEFTEST_BOT_USER_ID) {
                    $foundThread = $thread
                    $foundNote = $note
                    break
                }
            }
        }
        if ($foundNote) {
            break
        }
    }

    return @{
        thread = $foundThread
        note = $foundNote
    }
}

<#
    Create or update a thread on a merge request with any issues related to the merge.
#>
function Update-MergeRequestThread($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]

    $body = "### **`$``\textcolor{green}{\text{This merge request looks good.}}``$** `n`n"

    if ([array]$($MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks).Count -gt 0) {
        $body = "### **`$``\textcolor{red}{\text{I detected the following issues with this merge request:}}``$** `n`n"

        foreach ($issue in $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks) {
            $body += "- :x: **${issue}**`n"
        }
    }

    foreach ($issue in $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks) {
        $body += "- :heavy_check_mark: **${issue}**`n"
    }

    $body += "`nI will merge this request automatically when all issues are resolved, all pipelines are passing, and it is approved by someone.";

    $postBody = @{
        body = $body
    }

    $conversation = Find-ExistingMergeRequestThread $mergeRequest

    if ($conversation.note -ne $false) {
        if ($conversation.note.body -eq $body) {
            Write-Host "  - Thread note up-to-date, no need to comment."
        }
        else {
            $_response = Update-GitlabItem -Body $postBody -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/discussions/$($conversation.thread.id)/notes/$($conversation.note.id)"
        }
    }
    else {
        $_response = Create-GitlabItem -Body $postBody -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/discussions"

    }
}

<#
    This will mark a merge request thread/note from the bot as resolved.
#>
function Resolve-MergeRequestThread($mergeRequest, [bool]$resolved) {
    $project = $projectLookup[$mergeRequest.project_id]
    $conversation = Find-ExistingMergeRequestThread $mergeRequest

    if ($conversation.note -ne $false -and $conversation.thread -ne $false -and [bool]($conversation.note.resolved) -ne [bool]$resolved) {
        $postBody = @{
            resolved = $resolved
        }

        $conversation.thread | Format-List
        
        Write-Host "  - Updating merge request thread to status: $([bool]$resolved) because it was $([bool]($conversation.thread.resolved))"
        $_response = Update-GitlabItem -Body $postBody -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/discussions/$($conversation.thread.id)/notes/$($conversation.note.id)"
    }
    elseif ([bool]$conversation.note.resolved -eq [bool]$resolved) {
        Write-Host "  - Conversation status is up to date ($($conversation.note.resolved))"
    }
    else {
        Write-Host "  - Could not find existing conversation to resolve"
    }
}

$_PROJECT_APPROVAL_RULES_CACHE = @{}

function Get-ProjectApprovalRules($projectId) {
    $indexed = @{}
    if ($_PROJECT_APPROVAL_RULES_CACHE.ContainsKey($projectId)) {
        return $_PROJECT_APPROVAL_RULES_CACHE[$projectId]
    }

    $rules = Get-GitlabEndpointItems -endpoint "https://gitlab.com/api/v4/projects/${projectId}/approval_rules"

    foreach ($rule in $rules) {
        $indexed[$rule.id] = $rule
    }
    $_PROJECT_APPROVAL_RULES_CACHE[$projectId] = $indexed

    return $indexed
}

function Approve-MergeRequest($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    $postBody = @{
        sha = $mergeRequest.sha
    }

    $_response = Create-GitlabItem -Body $postBody -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/approve"
}

function Merge-MergeRequest($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    $postBody = @{
        sha = $mergeRequest.sha
    }

    $_response = Update-GitlabItem -Body $postBody -Uri "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/merge"
}