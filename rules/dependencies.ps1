function Check-PackageVersionExistsInRepo([string]$version, [string]$url) {
    # truncate 4th version section, so we are semver
    if (($version -split "\.").Count -gt 3 -and !($version.Contains('-'))) {
        $_s = ($version -split "\.");
        $_version = $_s[0] + "." + $_s[1] + "." + $_s[2]
        if ($version.Contains("-")) {
            $_version += $version.Substring($version.IndexOf("-"), $version.Length - $version.IndexOf("-"))
        }
        Write-Host "      - transformed version $version to $_version to be semver compatible for nuget apis"
        $version = $_version
    }

    try {
        $json = Invoke-WebRequest -Uri $url | ConvertFrom-Json

        foreach ($availableVersion in $json.versions) {
            if ($availableVersion -eq $version) {
                return $true
            }
        }
    } catch { }

    return $false
}

function Check-NuspecDependencies($project, $nuspecXml, $packageXml) {
    $startingFailCount = $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count
    Write-Host "  Found NuSpec Dependencies: "
    foreach ($nuspecDependency in $nuspecXml.GetElementsByTagName("dependency")) {
        Write-Host "    - $($nuspecDependency.id) // $($nuspecDependency.version)"
        
        # check that packages.config has the same dependency version
        $packageDependency = $packageXml.SelectSingleNode("//package[@id='$($nuspecDependency.id)']")
        if ($packageDependency -and $packageDependency.version -ne $nuspecDependency.version) {
            Write-Host "      - packages.config version differs from nuspec!: $($packageDependency.id) // $($packageDependency.version)"
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($packageDependency.id) nuspec version ($($nuspecDependency.version)) differs from packages.config version ($($packageDependency.version))"
        }
        elseif (!$packageDependency) {
            Write-Host "      - Could not find packages.config dependency! //package[@id='$($nuspecDependency.id)']"
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($nuspecDependency.id) is present in nuspec but not packages.config"
        }

        # check that nuget dependencies dont match non-beta prerelease tags when trying to merge to master
        $prereleaseTag = ""
        if ($nuspecDependency.version.Contains("-")) {
            $prereleaseTag = ($nuspecDependency.version -split "-")[1].Split(".")[0]
        }

        if ($mergeRequest.target_branch -eq "master" -and !($prereleaseTag -eq "" -or $prereleaseTag -eq "beta")) {
            Write-Host "      - Dependency is pointing to an alpha prerelease (${prereleaseTag}). You can only use beta prelease references when merging to master."
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Nuspec dependency $($nuspecDependency.id) is pointing to an alpha pre-release tag (${prereleaseTag}). You can only use beta prelease references when merging to master."
        }

        # check that nuget dependencies exist on the gitlab package repo when trying to merge to master
        $packageIsInRepo = $false

        # check that the package exists in nuget
        $packageIsInRepo = Check-PackageVersionExistsInRepo -Version $nuspecDependency.version -Url "https://api.nuget.org/v3-flatcontainer/$($nuspecDependency.id)/index.json"
        
        # check if it exists in gitlab package repo (only when merging to master)
        if ($packageIsInRepo -eq $false -and $mergeRequest.target_branch -eq "master") {
            $packageIsInRepo = Check-PackageVersionExistsInRepo -Version $nuspecDependency.version -Url "https://gitlab.com/api/v4/projects/33834162/packages/nuget/download/$($nuspecDependency.id)/index.json"
        }

        if (!$packageIsInRepo) {
            if ($mergeRequest.target_branch -eq "master") {
                Write-Host "      - not found in any nuget repo. (nuget / neftest gitlab)"
                $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($nuspecDependency.id) $($nuspecDependency.version) was not found in any nuget repo. (nuget / neftest gitlab)"
            }
            else {
                Write-Host "      - not found in the official nuget repo"
                $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($nuspecDependency.id) $($nuspecDependency.version) was not found in the official nuget repo"
            }
        }
    }

    return $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count -le $startingFailCount
}

function Check-PackagesConfigDependencies($project, $nuspecXml, $packageXml) {
    $startingFailCount = $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count
    Write-Host "  Found Packages.config Dependencies: "
    foreach ($packageDependency in $packageXml.GetElementsByTagName("package")) {
        Write-Host "    - $($packageDependency.id) // $($packageDependency.version)"

        if ($packageDependency.skipnuspec -or $packageDependency.developmentDependency) {
            continue
        }

        # check that nuspec has the same dependency version, this is somewhat redundant to above, but we need to at least make sure it exists
        $nuspecDependency = $nuspecXml.SelectSingleNode("//dependency[@id='$($packageDependency.id)']")
        if (!$nuspecDependency) {
            Write-Host "      - Dependency exists in packages.config but not nuspec! //dependency[@id='$($packageDependency.id)']"
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($packageDependency.id) is present in packages.config but not nuspec"
        }

        # check that dependencies dont match non-beta prerelease tags when trying to merge to master
        $prereleaseTag = ""
        if ($packageDependency.version.Contains("-")) {
            $prereleaseTag = ($packageDependency.version -split "-")[1].Split(".")[0]
        }

        if ($mergeRequest.target_branch -eq "master" -and !($prereleaseTag -eq "" -or $prereleaseTag -eq "beta")) {
            Write-Host "      - Dependency is pointing to an alpha prerelease (${prereleaseTag}). You can only use beta prelease references when merging to master."
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "packages.config dependency $($packageDependency.id) is pointing to an alpha pre-release tag (${prereleaseTag}). You can only use beta prelease references when merging to master."
        }
    }

    return $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count -le $startingFailCount
}

<#

This:
  - ensures that all nuspec dependencies exist in packages.config and versions match exactly
  - ensures that all packages.config packages exist in the nuspec (unless they have skipnuspec="true" attribute)

  On branches merging to master:
    - ensures there are no non-beta prelease packages referenced

  On master merging to release
    - ensures there are no prelease packages referenced

#>
function Check-ProjectDependencies($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    $repoPath = Get-MergeRequestRepoPath $mergeRequest

    Write-Host "  Reading packages from: ${repoPath}$($project.name)\packages.config"
    [xml]$packageXml = Get-Content -Path "${repoPath}$($project.name)\packages.config"

    Write-Host "  Reading nuspec dependencies from: ${repoPath}$($project.name).nuspec"
    [xml]$nuspecXml = Get-Content -Path "${repoPath}$($project.name).nuspec"
    
    if (Check-NuspecDependencies $project $nuspecXml $packageXml) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks += ".nuspec dependencies are valid"
    }

    if (Check-PackagesConfigDependencies $project $nuspecXml $packageXml) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks += "packages.config dependencies are valid"
    }
}

function Check-MergeRequestDependencies($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    Write-Host "Checking Merge Request: $($mergeRequest.title) in $($project.name)"

    CheckOut-GitlabBranch -MergeRequest $mergeRequest
    Check-ProjectDependencies -MergeRequest $mergeRequest
}
