﻿

function Check-MergeRequestApprovals($mergeRequest) {
    $projectRules = Get-ProjectApprovalRules $mergeRequest.project_id
    $approval = Get-GitlabItem "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/approvals"
    $needsRulesFulfilled = $false


    if (([array]$approval.approval_rules_left).Count -gt 0) {
        foreach ($rule in $approval.approval_rules_left) {
            $frull = $projectRules[$rule.id]
            $approvalsRequired = $frull.approvals_required;

            if (([array]$frull.eligible_approvers).Count -eq 1 -and [int]$frull.eligible_approvers[0].id -eq [int]$Env:NEFTEST_BOT_USER_ID) {
                continue;
            }

            $eligibleApprovers = @([array]$frull.eligible_approvers | %{ '@' + [string]$_.username })
            if ($eligibleApprovers.Count -gt 0) {
                $eligibleApprovers = $eligibleApprovers -join ', '
            }
            else {
                $eligibleApprovers = 'Anyone'
                $approvalsRequired -= 1
            }

            $neededRules += $frull
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Needs ${approvalsRequired} total approvals, from users: ${eligibleApprovers}"
        }
    }
    
    if ($neededRules.Count -eq 0) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks += "Has all needed approvals"
    }
}