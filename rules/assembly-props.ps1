﻿function Check-AssemblyInfo($mergeRequest) {
    $repoPath = Get-MergeRequestRepoPath $mergeRequest
    $passed = $true
    $assemblyVersionPath = "${repoPath}$($project.name)\Properties\AssemblyVersion.cs"

    # make sure AssemblyVersion.cs does not exist
    Write-Host "  - Checking for AssemblyVersion.cs"
    if (Test-Path -Path $assemblyVersionPath) {
        Write-Host "    - AssemblyVersion.cs at $($assemblyVersionPath) ! this should not be checked in to the repo!"
        $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "$($project.name)\Properties\AssemblyVersion.cs should not be checked in to the repo."
        $passed = $false
    }

    # make sure none of the assembly property files contain AssemblyVersion / AssemblyFileVersion / AssemblyInformationalVersion attributes
    $propertyFiles = Get-ChildItem "${repoPath}$($project.name)\Properties\"
    foreach ($propertyFile in $propertyFiles) {
        if ($propertyFile.Name -eq "AssemblyVersion.cs") {
            continue
        }

        Write-Host "  - Checking $($project.name)\Properties\$($propertyFile.Name) for Version Attributes"
        
        foreach($line in (Get-Content -Path $propertyFile.FullName)) {
            if($line.StartsWith("[assembly: AssemblyVersion(") -or $line.StartsWith("[assembly: AssemblyFileVersion(") -or $line.StartsWith("[assembly: AssemblyInformationalVersion(")) {
                Write-Host "    - $($project.name)\Properties\$($propertyFile.Name) should not contain Version attributes."
                $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "$($project.name)\Properties\$($propertyFile.Name) should not contain Version Attributes."
                $passed = $false
                break
            }
        }
    }


    if ($passed) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks += "AssemblyInfo is valid."
    }
}