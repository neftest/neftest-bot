﻿function Check-NuSpecDependency($MergeRequest, $Dependency) {
    Write-Host "    - Checking dependency $($Dependency.id) $($Dependency.version)"
    $prereleaseTag = ""
    if ($Dependency.version.Contains("-")) {
        $prereleaseTag = ($Dependency.version -split "-")[1].Split(".")[0]
    }
    
    if ($mergeRequest.target_branch -eq "master" -and !($prereleaseTag -eq "" -or $prereleaseTag -eq "beta")) {
        Write-Host "      - Dependency is pointing to an alpha prerelease (${prereleaseTag}). You can only use beta prelease references when merging to master."
        $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Nuspec dependency $($Dependency.id) is pointing to an alpha pre-release tag (${prereleaseTag}). You can only use beta prelease references when merging to master."
    }
    
    # check that nuget dependencies exist on the gitlab package repo when trying to merge to master
    $packageIsInRepo = $false

    # check that the package exists in nuget, this might fail and that is ok on non release branches
    $packageIsInRepo = Check-PackageVersionExistsInRepo -Version $Dependency.version -Url "https://api.nuget.org/v3-flatcontainer/$($Dependency.id)/index.json"
    
    if ($packageIsInRepo -ne $false) {
        Write-Host "      - Found in nuget package repo"
    }

    # check if it exists in gitlab package repo (only when merging to non release branches)
    if ($packageIsInRepo -eq $false -and $mergeRequest.target_branch -ne "release") {
        $packageIsInRepo = Check-PackageVersionExistsInRepo -Version $Dependency.version -Url "https://gitlab.com/api/v4/projects/33834162/packages/nuget/download/$($Dependency.id)/index.json"
        Write-Host "      - Found in gitlab package repo"
    }

    if ($packageIsInRepo) {
        if ($prereleaseTag -eq "beta") {
            Write-Host "      - PreRelease tag '${prereleaseTag}' is ok because this is a beta branch"
        }
    }
    else {
        if ($mergeRequest.target_branch -eq "master") {
            Write-Host "      - not found in any nuget repo. (nuget / neftest gitlab)"
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($Dependency.id) $($Dependency.version) was not found in any nuget repo. (nuget / neftest gitlab)"
        }
        else {
            Write-Host "      - not found in the official nuget repo"
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Dependency $($Dependency.id) $($Dependency.version) was not found in the official nuget repo"
        }
    }
}

function Check-NuPackageArtifact($MergeRequest, $Job) {
    Write-Host "  - Checking NuSpec: "
    $binPath = "$(Get-MergeRequestRepoPath $mergeRequest)$($project.name)\bin\"

    $nupkgs = Get-ChildItem -Path "${binPath}*.nupkg"

    if ($nupkgs.Count -eq 0) {
        $MERGEREQUEST_RESULTS[$MergeRequest.id].failedTasks += "Unable to find nupkg in $($project.name)\bin\"
        return
    }
    
    $tmpZipName = $nupkgs[0].FullName.Replace(".nupkg", ".zip")
    Move-Item -Path $nupkgs[0] -Destination $tmpZipName
    Expand-Archive -LiteralPath $tmpZipName -DestinationPath "${binPath}nupkg"

    
    $nuspecs = Get-ChildItem -Path "${binPath}nupkg\*.nuspec"

    if ($nuspecs.Count -eq 0) {
        $MERGEREQUEST_RESULTS[$MergeRequest.id].failedTasks += "Unable to find nuspec in extracted $($nupkgs[0].Name)"
        return
    }

    [xml]$nuspecXml = Get-Content -Path $nuspecs[0]
    
    foreach ($nuspecDependency in $nuspecXml.GetElementsByTagName("dependency")) {
        Check-NuSpecDependency -MergeRequest $mergeRequest -Dependency $nuspecDependency
    }
}

function Check-BuildArtifacts($MergeRequest, $Job) {
    $project = $projectLookup[$mergeRequest.project_id]
    $failedCount = $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks.Count

    # GET /projects/:id/jobs/artifacts/:ref_name/download?job=name
    $outFile = "$(Get-MergeRequestRepoPath $mergeRequest)artifacts.zip"
    Invoke-WebRequest -Uri "https://gitlab.com/api/v4/projects/$($project.id)/jobs/artifacts/$($mergeRequest.source_branch)/download?job=build" -OutFile $outFile

    Expand-Archive -LiteralPath $outFile -DestinationPath $(Get-MergeRequestRepoPath $mergeRequest)

    Check-NuPackageArtifact -MergeRequest $mergeRequest

    if ($MERGEREQUEST_RESULTS[$MergeRequest.id].failedTasks.Count -eq $failedCount) {
        $MERGEREQUEST_RESULTS[$MergeRequest.id].passedTasks += "Build artifacts look good for $($MergeRequest.sha)"
    }
}

function Check-PipelineArtifacts($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    $binPath = "$(Get-MergeRequestRepoPath $mergeRequest)$($project.name)\bin\"

    if ((Test-Path -Path $binPath)) {
        Remove-Item -Force -Confirm:$false -Recurse -Path $binPath
    }

    $jobs = Get-GitlabEndpointItems "https://gitlab.com/api/v4/projects/$($project.id)/pipelines/$($MERGEREQUEST_RESULTS[$mergeRequest.id].pipeline.id)/jobs?scope=success&include_retried=true"
    $foundBuildArtifacts = $false;

    foreach ($job in $jobs) {
        if ($job.name -eq 'build' -and $job.commit.id -eq $mergeRequest.sha) {
            $foundBuildArtifacts = $true
            Check-BuildArtifacts -MergeRequest $mergeRequest -Job $job
            break
        }
    }

    if ($foundBuildArtifacts -eq $false) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Could not find build artifacts for $($mergeRequest.sha)"
    }
}

function Check-MergeRequestPipelines($mergeRequest) {
    $project = $projectLookup[$mergeRequest.project_id]
    $passing = $false
    $found = $false
    $pipelines = Get-GitlabEndpointItems "https://gitlab.com/api/v4/projects/$($project.id)/merge_requests/$($mergeRequest.iid)/pipelines"

    foreach ($pipeline in $pipelines) {
        if ($pipeline.sha -eq $mergeRequest.sha -and $mergeRequest.source_branch -eq $pipeline.ref) {
            $found = $true
            $MERGEREQUEST_RESULTS[$mergeRequest.id].pipeline = $pipeline
            if ($pipeline.status -eq 'success') {
                $passing = $true
            }
            break
        }
    }

    if ($passing) {
        $MERGEREQUEST_RESULTS[$mergeRequest.id].passedTasks += "Pipeline is passing for $($mergeRequest.sha)"
    }
    else {
        if ($found) {
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "Pipeline status for $($mergeRequest.sha) is: $($pipeline.status)"
        }
        else {
            $MERGEREQUEST_RESULTS[$mergeRequest.id].failedTasks += "No pipeline found for $($mergeRequest.sha)"
        }
    }
}